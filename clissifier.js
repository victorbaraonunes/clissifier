#!/usr/bin/env node

'use strict';

const program = require('commander');
const chalk = require('chalk');
const path = require('path');
const fs = require('fs');
const opencv = require('opencv');
const _ = require('lodash');
const rm = require('rimraf');

const OUTPUT_DIR = path.join(__dirname, 'output');
const SUPPORTED_EXTENSIONS = ['.jpg', '.png', '.bmp'];
const THICKNESS = 1;
const LINE_TYPE = 8;
const WEIGHTS = [1, 2, 4, 8, 16, 32, 64, 128];

program
	.version('1.0.0')
	.description('A CLI tool to classify images')
	.option('-p, --path <path>', 'path to folder of files to classify')
	.parse(process.argv);

if (!program.path || !program.path.length) {
	program.help();
	process.exit(0);
}

const FOLDER_PATH = path.join(__dirname, program.path);

console.log(chalk.blue('Classifying '+ SUPPORTED_EXTENSIONS +' files on folder: ') 
	+ chalk.yellow(FOLDER_PATH));

let files = fs.readdir(FOLDER_PATH, (err, files) => {
	if (err) {
		console.log(chalk.red('Error reading files: ') + chalk.bgRed(err));
		process.exit(0);
	}

	// only start processing if the folder isn't empty
	if (!files || !files.length) {
		console.log(chalk.red('The informed folder is empty!'));
		process.exit(0);	
	}

	// clean output dir and create it again!
	rm.sync(OUTPUT_DIR);
	fs.mkdirSync(OUTPUT_DIR);

	let filesProcessed = 0
		, errors = []
		, fileData = [];

	_.each(files, (file) => {
		let filePath = path.join(FOLDER_PATH, file);
		console.log(chalk.blue('Processing file: ') 
			+ chalk.yellow(filePath));

		// process only image files
		if (!_.includes(SUPPORTED_EXTENSIONS, path.extname(filePath))) return;

		opencv.readImage(filePath, (err, img) => {
			if (err) return errors.push(err);

			let width = img.width()
				, height = img.height()
				, imgPixels = (width * height);

			let maxVertical = (height / 2)
				, maxHorizontal = (width / 2);

			// matrix
			let histogram = {
				q1: {
					r: new Array(256).fill(0)
					, g: new Array(256).fill(0)
					, b: new Array(256).fill(0)
				}
				, q2: {
					r: new Array(256).fill(0)
					, g: new Array(256).fill(0)
					, b: new Array(256).fill(0)
				}
				, q3: {
					r: new Array(256).fill(0)
					, g: new Array(256).fill(0)
					, b: new Array(256).fill(0)
				}
				, q4: {
					r: new Array(256).fill(0)
					, g: new Array(256).fill(0)
					, b: new Array(256).fill(0)
				}
			};

			// LBP
			let lbpImage = img.copy();
			lbpImage.convertGrayscale();

			let lbp = new opencv.Matrix(img.height(), img.width());

			// quadrant histogram
			for (let row = 0; row < img.height(); row++) {
				for (let col = 0; col < img.width(); col++) {
					let pixel = img.getPixel(row, col);

					if (row < maxVertical && col < maxHorizontal) {
						// Q1
						histogram.q1.r[pixel[2]]++;
						histogram.q1.g[pixel[1]]++;
						histogram.q1.b[pixel[0]]++;
					} else if (row < maxVertical && col > maxHorizontal) {
						// Q2
						histogram.q2.r[pixel[2]]++;
						histogram.q2.g[pixel[1]]++;
						histogram.q2.b[pixel[0]]++;
					} else if (row > maxVertical && col < maxHorizontal) {
						// Q3
						histogram.q3.r[pixel[2]]++;
						histogram.q3.g[pixel[1]]++;
						histogram.q3.b[pixel[0]]++;
					} else if (row > maxVertical && col > maxHorizontal) {
						// Q4
						histogram.q4.r[pixel[2]]++;
						histogram.q4.g[pixel[1]]++;
						histogram.q4.b[pixel[0]]++;
					}

					let center = lbpImage.getPixel(row, col)
						, topLeft = lbpImage.getPixel(row - 1, col - 1) || 0
						, topUp = lbpImage.getPixel(row - 1, col) || 0
						, topRight = lbpImage.getPixel(row - 1, col + 1) || 0
						, right = lbpImage.getPixel(row, col + 1) || 0
						, left = lbpImage.getPixel(row, col - 1) || 0
						, bottomLeft = lbpImage.getPixel(row + 1, col - 1) || 0
						, bottomRight = lbpImage.getPixel(row + 1, col + 1) || 0
						, bottomDown = lbpImage.getPixel(row + 1, col) || 0;

					let neighbors = [
						topLeft
						, topUp
						, topRight
						, right
						, left
						, bottomLeft
						, bottomRight
						, bottomDown
					];

					let values = _.map(neighbors, (value) => {
						return (value >= center ? 1 : 0);
					});

					let newValue = 0;
					for (let value = 0; value < values.length; value++) {
						newValue += WEIGHTS[value] * values[value];
					}

					lbp.set(row, col, newValue);
				}
			}

			lbpImage.release();

			// histogram
			let lbpHistogram = opencv.histogram.calcHist(
				lbp
				, [2]
				, [256]
				, [[0, 256]]
				, true
			);

			// normalize the histogram
			let normalizedHistogram = _.map(histogram, (quadrant) => {
				return _.map(quadrant, (channel) => {
					return _.map(channel, (value) => {
						return (value / (imgPixels / 4));
					});
				});
			});

			let percentiles = _.map(normalizedHistogram, (quadrant) => {
				return _.map(quadrant, (channel) => {
					let total = _.reduce(channel, (sum, value) => sum + value, 0)
						, percentiles = [];

					let parts = 10
						, sum = 0;

					for (let j = 0; j < channel.length; j++) {
						let max = (total / parts)
							, value = channel[j];

						sum += value;

						if (!percentiles.length && sum > 0) {
							percentiles.push((j == 0 ? j : j - 1));
						}

						if (sum >= max) {
							parts--;
							percentiles.push(j);
						}
					}

					return percentiles;
				});
			});

			let params = _.map(percentiles, (quadrant) => {
				return _.map(quadrant, (channel) => {
					let params = [];

					for (let i = 1; i < channel.length; i++) {
						let param = (channel[i] - channel[i - 1]) / 255;
						params.push(param);
					}

					return params;
				});
			});

			let category = 'foods';

			if (filesProcessed < 100) {
				category = 'aboriginal';
			} else if (filesProcessed < 200) {
				category = 'beaches';
			} else if (filesProcessed < 300) {
				category = 'architecture';
			} else if (filesProcessed < 400) {
				category = 'buses';
			} else if (filesProcessed < 500) {
				category = 'dinosaurs';
			} else if (filesProcessed < 600) {
				category = 'elephants';
			} else if (filesProcessed < 700) {
				category = 'flowers';
			} else if (filesProcessed < 800) {
				category = 'horses';
			} else if (filesProcessed < 900) {
				category = 'mountains';
			}

			let data = {
				category
				, params: lbpHistogram.concat(_.flattenDeep(params))
			};

			filesProcessed++;

			fileData.push(data);

			lbp.release();
			img.release();
		});
	});

	let file = '@relation images\n';

	for (let i = 1; i <= 376; i++) {
		file += '\n@attribute a'+ i +' Numeric';
	}

	file += '\n@attribute class {aboriginal,beaches,architecture,buses,dinosaurs,elephants,flowers,horses,mountains,foods}';
	file += '\n\n@data';

	_.each(fileData, (fileData) => {
		file += '\n';

		_.each(fileData.params, (param) => {
			file += param + ', ';
		});

		file += '\'' + fileData.category + '\'';
	});

	file += '\n';

	// write file
	fs.writeFile(path.join(OUTPUT_DIR, 'color.arff'), file, (err) => {
		console.log(chalk.blue('ARFF file written on: ') 
			+ chalk.yellow(OUTPUT_DIR + '/color.arff'));
	});

	console.log(chalk.blue(filesProcessed) + chalk.blue(' processed file(s)!'));
});

// helper functions

function getFileNameWithoutExtension(filePath) {
	return path.basename(filePath, path.extname(filePath));
}
